angular.module('starter', ['ionic', 'parse-starter.controllers', 'parse-starter.factories'])
  .config(function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('home');
    $stateProvider
      
      .state('home', {
        url: ('/home'),
        templateUrl: ('/home.html'),
        //controller: 'HomeCtrl'
      })
      
      .state('modalcrtl',{
        url:('modalcrtl')
        templateUrl: 'templates/my-modal.html',
        controller: 'ModalCtrl'
     
      .state('mapui', {
        url: '/mapui',
        templateUrl: 'templates/mapUI.html'
        
        })
        .state('usercards', {
        url: '/usercards',
        templateUrl: 'templates/usercards.html'
        
      })
  })
  .run(function ($ionicPlatform, $state) {

    Parse.initialize("bIwxO7Maintenance04W2hlOfuh2HYw9ptBNWdwfda5rqbp5ZHG", "KFvMB0RgoVC6ydNwjb3F8vhyfO2Q6KwRwVnjEk2o");

    //Todo
    //window.fbAsyncInit = function () {
    //  Parse.FacebookUtils.init({ // this line replaces FB.init({
    //    appId: 'FB_APP_ID', // Facebook App ID
    //    //   status: true,  // check Facebook Login status
    //    cookie: true,  // enable cookies to allow Parse to access the session
    //    xfbml: true,  // initialize Facebook social plugins on the page
    //    version: 'v2.2' // point to the latest Facebook Graph API version
    //  });
    //
    //};
    //
    //(function (d, s, id) {
    //  var js, fjs = d.getElementsByTagName(s)[0];
    //  if (d.getElementById(id)) {
    //    return;
    //  }
    //  js = d.createElement(s);
    //  js.id = id;
    //  js.src = "https://connect.facebook.net/en_US/sdk.js";
    //  fjs.parentNode.insertBefore(js, fjs);
    //}(document, 'script', 'facebook-jssdk'));

    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if (Parse.User.current()) {
        $state.go('home')

      }
    });
  });