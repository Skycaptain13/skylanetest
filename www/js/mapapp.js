angular.module('parse-starter.controllers')
  .controller('MapCtrl', function () {
var map;
var infowindow;

function initMap() {
  var userLoc = {lng:-80.8966,
  lat: 34.0298};

  map = new google.maps.Map(document.getElementById('map'), {
    center: userLoc,
    zoom: 10
  });

  infowindow = new google.maps.InfoWindow();

  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: userLoc,
    radius: 50000,
    types: ['airport'],
    keyword: ['aviation']
  }, callback);
}

function callback(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

   
    });